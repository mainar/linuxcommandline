Acknowledgements
----------------

Handouts provided by `EMBL Heidelberg <http://www.embl.de>`_ Photolab (Many thanks to Udo Ringeisen)

The git server at git.embl.de is maintained by Frank Thommen.

EMBL Logo © `EMBL Heidelberg <http://www.embl.de>`_

**License**:
`CC BY-SA 3.0 <http://creativecommons.org/licenses/by-sa/3.0/>`_
