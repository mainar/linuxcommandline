Linux Commandline Course
========================

This repository contains material for teaching linux commandline & git.
See file acknowledgements.rst for contributors

The folders ''linux_beginner'' and ''linux_intermediate'' are Re-Writes of the documentation 
which was originally done in Word, converted to ReStructuredText.

The folders ''git_beginner'' and ''gitlab_embl'' contain materials for the Git Fundamentals course and an associated seminar introducing
the EMBL GitLab system.

Here, we're using `ReStructuredText <http://docutils.sourceforge.net/rst.html>`_ for writing, and `Sphinx <http://sphinx-doc.org/>`_
to convert the Markup into HTML and PDF ("Sphinx is a tool that makes it easy to create intelligent and beautiful documentation") 
